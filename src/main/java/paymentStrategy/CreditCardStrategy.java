package paymentStrategy;

public class CreditCardStrategy implements PaymentStrategy {

    private String name;
    private String cardNumber;
    private String cvv2;
    private String dateOfExpire;

    public CreditCardStrategy(String name, String cardNumber, String cvv2, String dateOfExpire) {
        this.name = name;
        this.cardNumber = cardNumber;
        this.cvv2 = cvv2;
        this.dateOfExpire = dateOfExpire;
    }


    @Override
    public void pay(int amount) {
        System.out.println(amount +" paid with credit/debit card");
    }
}
