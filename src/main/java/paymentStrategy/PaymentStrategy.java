package paymentStrategy;

public interface PaymentStrategy {
    void pay(int amount);
}
