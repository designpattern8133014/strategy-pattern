package paymentStrategy;

public class Test {

    public static void main(String[] args) {
        ShoppingCart cart = new ShoppingCart();

        Item item1 = new Item("1235678987664", 100000);
        Item item2 = new Item("5678234565433", 40000);
        cart.addItem(item1);
        cart.addItem(item2);

        cart.pay(new PaypalStrategy("taraneh.ranjbar@gmail.com","P@ssw0rd"));
        cart.pay(new CreditCardStrategy("taraneh R","1234567890987654","432","0506"));
    }





}
