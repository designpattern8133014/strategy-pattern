package hashStrategy;

import org.apache.commons.codec.digest.DigestUtils;

public class Sha256Hash implements HashStrategy {
    @Override
    public byte[] hash(String raw) {
        return DigestUtils.sha256(raw);
    }
}
