package hashStrategy;

import org.apache.commons.codec.digest.DigestUtils;

public class Test {

    public static void main(String[] args) {
        SecureContent secureContent = new SecureContent("P@ssw0rd");
        byte[] bytes = secureContent.hashContent(new Md5Hash());
        System.out.println(bytes);
        byte[] bytes1 = secureContent.hashContent(new Sha256Hash());
        System.out.println(bytes1);

        secureContent.hashContent(raw ->
            DigestUtils.sha1(raw));
    }
}
