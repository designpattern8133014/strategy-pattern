package hashStrategy;

@FunctionalInterface
public interface HashStrategy {

    byte[] hash(String raw);
}
